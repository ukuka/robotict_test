import { Template } from 'meteor/templating';
import { getValues } from './function';

import './main.html';

Template.main.helpers({
  values() {
    return getValues();
  },
});