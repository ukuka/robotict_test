export function getValues() {
  return Array.from(Array(100), (_, i) => {
    i++;
    if (i % 3 === 0 && i % 5 === 0) {
      return 'RobotICT';
    } else if (i % 3 === 0) {
      return 'Robot';
    } else if (i % 5 === 0) {
      return 'ICT';
    }
    return i;
  });
}